<?php
// 回复信息配置
// 统一的管理项目操作过程中需要返回用户信息的 内容
// 数据库
return [
    'code' => [
        'SUCCESS'                     => 1,
        'USER_LOGIN_VALIDATE_ERROR'   => -1000,
        // user信息
        'ERROR_NO_USER'               => -1001,
        'ERROR_USER_START'            => -1002,
        'ERROR_PASSWORD'              => -1003,
        'ERROR_LOGIN_EXCESS_TIME_OUT' => -1005,
    ],
    'info' => [
        1     => '操作成功',
        -1000 => '用户登入校验不成功',
        -1001 => '用户不存在',
        -1002 => '用户状态不对',
        -1003 => '用户密码错误',
        -1005 => '登入超过规定次数'
    ]
];
