<?php
//为了方便与管理自定义门面类和别名 就定义在配置文件中
return [
    'facade' => [
        \data\facade\SC::class => \data\util\SC::class,
        \data\facade\OnlyLogin::class => \data\util\OnlyLogin::class,
    ],
    'alias' => [
        'SC' => \data\facade\SC::class,
        'OnlyLogin' => \data\facade\OnlyLogin::class,
    ]
];
