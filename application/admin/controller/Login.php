<?php

namespace app\admin\controller;

use think\Controller;
use Request,Log;
use data\service\UserService;
use think\captcha\Captcha;
use data\validate\UserValidata;

class Login extends Controller
{
    private $userService;
    /**
     * 控制器的初始化方法
     */
    protected function initialize()
    {
        // 该方法和构造函数是类似
        $this->userService = new UserService;
    }
    /**
     * @route('login')
     */
    public function login()
    {
        if (Request::isPost()) {
            // 建议基础的学员 ，先做一个测试
            // Log::write(Request::param());
            $username = Request::param('username');
            $password = Request::param('password');
            $userValidate = new UserValidata;
            if (!$userValidate->check(Request::param())) {
                return json([
                    'code' => USER_LOGIN_VALIDATE_ERROR,
                    'msg'  => $userValidate->getError() // 验证器回复错误信息的方法
                ]);
            }
            return ajaxRuturn($this->userService->login($username,$password));
        }
        // 渲染页面
        // 默认 admin\view\login\login.html
        return $this->fetch();
    }

    public function verify()
    {
        $captcha = new Captcha();
        return $captcha->entry();
    }
}
