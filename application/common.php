<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
use think\Loader;
Loader::addNamespace('data', Loader::getRootPath() . 'data' . DIRECTORY_SEPARATOR);
/**
 * 规定回复信息的格式的方法
 * @param  integer $code 信息码
 * @param  array  $data [description]
 * @return [type]       [description]
 */
function ajaxRuturn($code, $data = [])
{
    // $result = ['code' => 1, 'message' => '操作成功'];
    $result = ['code' => $code, 'msg' => getMessage($code)];
    $result = (!empty($data)) ? $result['data'] = $data : $result;
    return json($result);

    // $result = ['code' => $code, 'msg' => getMessage($code)];
    // return json($result);
}

/**
 * 获取信息码的稍息
 * @param  integer $code信息码
 * @return string       信息对应的稍息
 */
function getMessage($code){
    $info = config('message.info'); // array 数组
    // 先判断是否存在这个键 array_key_exists
    // 如果存在就获取信息
    // 如果不存在就返回  操作失败
    return (array_key_exists($code, $info)) ? $info[$code] : '操作失败';
}





//
