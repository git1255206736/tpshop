<?php

namespace data\model;

use think\Model;

class Test extends Model
{

      /**
       * 数据表主键 复合主键使用数组定义
       * @var string|array
       */
      protected $pk = 'id';
}
