<?php

namespace data\model;

use think\Model;

class User extends Model
{
      /**
       * 数据表主键 复合主键使用数组定义
       * @var string|array
       */
      protected $pk = 'id';

      public function userRole()
      {
          // 注意 return 一定是需要的
          return $this->hasOne('UserRole', 'role_id', 'role_id');
      }
}
