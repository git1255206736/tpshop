<?php

namespace data\validate;

use think\Validate;

/**
 * 使用验证器校验用户输入的验证码
 */
class UserValidata extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
		protected $rule = [
				// 用户名和 密码  省...
				// 关于验证器的一些参数的使用
				'vertify' => 'require|captcha'
		];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
				'vertify' => [
						'require' => '验证码必须有',
						'captcha' => '验证码错误',
				]
		];
}
