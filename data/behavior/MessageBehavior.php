<?php

namespace data\behavior;

use Config;

class MessageBehavior
{
    /**
     * 信息码注册
     */
    public function run()
    {
        $codes = Config::get('message.code');
        foreach ($codes as $key => $value) {
            define($key, $value);
        }
    }
}
